create user anna_nowak with password 'postgis';
create role gis;
grant gis to jan_kowalski;
grant gis to anna_nowak;

alter table powiaty enable row level security;
grant select on powiaty to gis;

create policy powiaty_gis_select
on powiaty
for select
to gis
using (current_user = account);

alter table kolej enable row level security;

create policy kolej_gis_edit
on kolej
for all
to gis
using (st_within(geom, (
select st_collect(geom) from powiaty
)));

grant all on kolej to gis;

create policy kolej_gis_select
on kolej
for select
to gis
using (st_within(geom, (
select st_collect(geom) from powiaty
)));
