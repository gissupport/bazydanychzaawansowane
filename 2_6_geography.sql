SELECT a.* FROM ne_10m_lakes a, ne_10m_rivers b WHERE b.name = 'Kemijoki' AND ST_Intersects(a.geom, b.geom);

SELECT * FROM ne_10m_layers WHERE ST_Intersects(geom, ST_SetSRID(ST_MakePoint(-86,45.5),4326));

SELECT ST_Area(geom) FROM ne_10m_lakes WHERE name = 'Lake Ontario';
SELECT ST_Length(geom) FROM ne_10m_rivers WHERE name = 'Kemijoki';

ALTER TABLE ne_10m_lakes ADD COLUMN geog GEOGRAPHY;
ALTER TABLE ne_10m_rivers ADD COLUMN geog GEOGRAPHY;
UPDATE ne_10m_lakes SET geog = geom::geography;
UPDATE ne_10m_rivers SET geog = geom::geography;
CREATE INDEX ON ne_10m_lakes USING gist(geog);
CREATE INDEX ON ne_10m_rivers USING gist(geog);


SELECT a.* FROM ne_10m_lakes a, ne_10m_rivers b WHERE b.name = 'Kemijoki' AND ST_Intersects(a.geog, b.geog);

SELECT * FROM ne_10m_layers WHERE ST_Intersects(geog, ST_SetSRID(ST_MakePoint(-86,45.5),4326));
SELECT * FROM ne_10m_layers WHERE ST_Dwithin(geog, ST_SetSRID(ST_MakePoint(-86,45.5),4326),500000);


SELECT ST_Area(geog) FROM ne_10m_lakes WHERE name = 'Lake Ontario';
SELECT ST_Length(geog) FROM ne_10m_rivers WHERE name = 'Kemijoki';

SELECT ST_Area(geom::geography) FROM ne_10m_lakes WHERE name = 'Lake Ontario';
