SELECT r.name,
    r.highway,
        CASE
            WHEN st_coveredby(r.wkb_geometry, m.wkb_geometry) THEN r.wkb_geometry
            ELSE st_intersection(r.wkb_geometry, m.wkb_geometry)
        END AS geom
   FROM multipolygons m,
    lines r
  WHERE m.name = 'powiat nyski' AND (r.highway = ANY (ARRAY['primary', 'primary_link', 'secondary', 'secondary_link', 'tertiary', 'residential', 'unclassified', 'minor', 'road', 'living_street'])) AND st_intersects(r.wkb_geometry, m.wkb_geometry);
  
CREATE MATERIALIZED VIEW drogi_nyski_mv AS
SELECT r.name,
    r.highway,
        CASE
            WHEN st_coveredby(r.wkb_geometry, m.wkb_geometry) THEN r.wkb_geometry
            ELSE st_intersection(r.wkb_geometry, m.wkb_geometry)
        END AS geom
   FROM multipolygons m,
    lines r
  WHERE m.name = 'powiat nyski' AND (r.highway = ANY (ARRAY['primary', 'primary_link', 'secondary', 'secondary_link', 'tertiary', 'residential', 'unclassified', 'minor', 'road', 'living_street'])) AND st_intersects(r.wkb_geometry, m.wkb_geometry);
  
 REFRESH MATERIALIZED VIEW drogi_nyski_mv;