SELECT date_at, ask_usd, row_number() over () AS rownum FROM gold;
SELECT date_at, ask_usd, row_number() over (order by date_at) AS rownum FROM gold;

SELECT date_at, ask_usd, lag(ask_usd) over (order by date_at) FROM gold;


SELECT date_at, ask_usd, lag(ask_usd) over (order by date_at),
ask_usd - lag(ask_usd) over (order by date_at) AS diff
FROM gold;

SELECT date_at, ask_usd, lag(ask_usd) over (order by date_at),
ask_usd - lag(ask_usd) over (order by date_at) AS diff,
round(((ask_usd - lag(ask_usd) over (order by date_at)) / ask_usd)*100,2) AS pdiff 
FROM gold;

SELECT date_at, ask_usd, lag(ask_usd) over (order by date_at),
ask_usd - lag(ask_usd) over (order by date_at) AS diff,
round(((ask_usd - lag(ask_usd) over (order by date_at)) / ask_usd)*100,2) AS pdiff,
round(avg(ask_usd) over(order by date_at ROWS BETWEEN 29 PRECEDING AND CURRENT ROW),2) AS "30d_ma"
FROM gold;

WITH prices AS (
SELECT date_at, ask_usd, lag(ask_usd) over (order by date_at),
ask_usd - lag(ask_usd) over (order by date_at) AS diff,
round(((ask_usd - lag(ask_usd) over (order by date_at)) / ask_usd)*100,2) AS pdiff,
round(avg(ask_usd) over(order by date_at ROWS BETWEEN 29 PRECEDING AND CURRENT ROW),2) AS "30d_ma"
FROM gold
)
SELECT d::date, p.ask_usd, p.lag FROM generate_series (
'2016-01-01'::date,
'2020-05-07'::date,
'1 day'::interval
) d LEFT JOIN prices p ON (d::date = p.date_at);