SELECT ST_VoronoiPolygons(ST_Collect(wkb_geometry)) FROM points
WHERE place='town' OR place='city';


SELECT (ST_Dump(ST_VoronoiPolygons(ST_Collect(wkb_geometry)))).geom FROM points
WHERE place = 'town' OR place='city';

WITH voronoi AS (SELECT (ST_Dump(ST_VoronoiPolygons(ST_Collect(wkb_geometry)))).geom FROM points
WHERE place = 'town' OR place='city')

SELECT p.osm_id, p.place, v.geom
FROM points p, voronoi v 
WHERE p.place IN ('town','city') AND ST_Intersects(p.wkb_geometry, v.geom);