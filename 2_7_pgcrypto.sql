CREATE EXTENSION pgcrypto;

ALTER TABLE gold ADD COLUMN our_price BYTEA;

UPDATE gold SET our_price = pgp_sym_encrypt((ask_usd*0.95)::text, 'sezamie otwórz się');

SELECT date_at, ask_usd, pgp_sym_decrypt(our_price, 'sezamie otwórz się') FROM gold;

SET SESSION my.vars.cryptokey = 'sezamie otwórz się';

SELECT date_at, ask_usd, pgp_sym_decrypt(our_price, current_setting('my.vars.cryptokey')) AS our_price FROM gold;

CREATE VIEW gold_decrypt AS SELECT date_at, ask_usd, pgp_sym_decrypt(our_price, current_setting('my.vars.cryptokey')) AS our_price FROM gold;

SELECT * FROM gold_decrypt;
