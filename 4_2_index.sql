SELECT * FROM miejscowosci WHERE nazwaglowna = 'Zimna Wódka';

EXPLAIN SELECT * FROM miejscowosci WHERE nazwaglowna = 'Zimna Wódka';

EXPLAIN ANALYZE SELECT * FROM miejscowosci WHERE nazwaglowna = 'Zimna Wódka';

CREATE INDEX ON miejscowosci(nazwaglowna);

EXPLAIN ANALYZE SELECT * FROM miejscowosci WHERE nazwaglowna LIKE 'Zimna%';

CREATE INDEX ON miejscowosci USING BTREE (nazwaglowna varchar_pattern_ops);

CREATE EXTENSION pg_trgm;

CREATE INDEX ON miejscowosci USING GIST(nazwaglowna gist_trgm_ops);

EXPLAIN ANALYZE SELECT * FROM miejscowosci WHERE ST_Dwithin(geom, ST_GeomFromEWKT('SRID=2180;POINT(333333 333333)'),5000);

EXPLAIN ANALYZE SELECT * FROM obiekty_fizjograficzne WHERE ST_Dwithin(geom, ST_GeomFromEWKT('SRID=2180;POINT(333333 333333)'),5000);

CREATE INDEX ON miejscowosci USING gist(geom);

SELECT pg_size_pretty(pg_relation_size('miejscowosci_geom_idx'));

SELECT pg_size_pretty(pg_relation_size('miejscowosci_nazwaglowna_idx'));
SELECT pg_size_pretty(pg_relation_size('miejscowosci_nazwaglowna_idx1'));

SELECT pg_size_pretty(pg_relation_size('miejscowosci'));



