SELECT name, osm_id, wkb_geometry, other_tags ->'wikipedia' as wikipedia FROM other_relations;

SELECT name, osm_id, wkb_geometry, other_tags ->'wikipedia' as wikipedia FROM other_relations
WHERE other_tags ? 'wikipedia';

SELECT name, osm_id, wkb_geometry, other_tags ->'wikipedia' as wikipedia FROM other_relations
WHERE other_tags ? 'wikipedia' AND other_tags->'wikipedia' LIKE 'pl%';

SELECT data->'stationNale' FROM stacje_monitoringu;
SELECT data->'city'->'commune'->>'communeName' FROM stacje_monitoringu;
SELECT data->'city'->'commune'->>'communeName', (data->>'gegrLon')::numeric AS lon FROM stacje_monitoringu;

SELECT row_to_json(gold) FROM gold;
SELECT json_agg(row_to_json(gold)) FROM gold;

SELECT json_build_object('data', date_at, 'cena', ask_usd) FROM gold;


