WITH RECURSIVE organization AS (
SELECT *, 1 AS level FROM "Employee" WHERE "ReportsTo" is null
UNION SELECT a.*, b.level+1 FROM "Employee" a, organization b WHERE a."ReportsTo" = b."EmployeeId"
)
SELECT * FROM organization;

WITH RECURSIVE organization AS (
SELECT *, 1 AS level, "FirstName" || ' ' || "LastName" AS path FROM "Employee" WHERE "ReportsTo" is null
UNION SELECT a.*, b.level+1, a."FirstName" || ' ' || a."LastName" || '<' || b.path 
FROM "Employee" a, organization b WHERE a."ReportsTo" = b."EmployeeId"
)
SELECT * FROM organization;