SELECT * FROM points WHERE place in('town','city','village');

SELECT osm_id, name, wkb_geometry, st_clusterkmeans(wkb_geometry, 10) over ()
FROM points WHERE place in('town','city','village');

SELECT st_clusterkmeans, st_collect(wkb_geometry) from (
SELECT osm_id, name, wkb_geometry, st_clusterkmeans(wkb_geometry,10) over ()
FROM points WHERE place in('town','city','village')
) k GROUP BY ST_Clusterkmeans;

SELECT st_clusterkmeans, st_convexhull(st_collect(wkb_geometry)) from (
SELECT osm_id, name, wkb_geometry, st_clusterkmeans(wkb_geometry,10) over ()
FROM points WHERE place in('town','city','village')
) k GROUP BY ST_Clusterkmeans;

SELECT osm_id, name, wkb_geometry, st_clusterdbscan(wkb_geometry, eps :=3000, minpoints := 2) over ()
FROM points WHERE place in('town','city','village');

SELECT unnest(ST_ClusterWithin(wkb_geometry,3000)) FROM points WHERE place in('town','city','village');
SELECT st_collectionextract(unnest(ST_ClusterWithin(wkb_geometry,3000)),1) FROM points WHERE place in('town','city','village');
